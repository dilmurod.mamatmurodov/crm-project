from django.db import models


class Service(models.Model):
    title = models.CharField(max_length=20)
    price = models.IntegerField(default=10)

    def __str__(self):
        return self.title


class Client(models.Model):
    full_name = models.CharField(max_length=110, default='')
    service_type = models.ForeignKey(Service, on_delete=models.SET_NULL, null=True)
    address = models.TextField(max_length=250)
    phone_number = models.CharField(max_length=60)
    create_time = models.DateTimeField(auto_now=True, auto_now_add=False)
    status = models.BooleanField(default=False)

    def __str__(self):
        return self.full_name


