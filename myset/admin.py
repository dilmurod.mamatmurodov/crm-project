from django.contrib import admin

# Register your models here.
from .models import (
    Service,
    Client,
)


class ServiceAdmin(admin.ModelAdmin):
    list_display = ['title', 'price',]


class ClientsAdmin(admin.ModelAdmin):
    list_display = ['service_type']


admin.site.register(Service, ServiceAdmin)
admin.site.register(Client, ClientsAdmin)