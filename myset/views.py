from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404


from .models import Client


def home_page(request):
    queryset = Client.objects.all()
    context = {
        "object_list": queryset,
        "title": "Home"
    }
    return render(request, "page/home.html", context)


