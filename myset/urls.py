from django.conf.urls import url, include
from django.contrib import admin
from .models import Client
from rest_framework import routers, serializers, viewsets


class ClientSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Client
        fields = ('full_name', 'phone_number')


class ClientViewSet(viewsets.ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


routers = routers.DefaultRouter()
routers.register(r'clients', ClientViewSet)


urlpatterns = [
    url(r'^', include(routers.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]